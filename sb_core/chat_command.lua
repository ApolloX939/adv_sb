
adv_sb.temp.cmds = {
    "help" = function (name, args)
        -- minetest.chat_send_player(name, "")
        if #args == 0 then
            minetest.chat_send_player(name, "- Advanced Skyblocks -")
            minetest.chat_send_player(name, "/island and /is are interchangable")
            minetest.chat_send_player(name, "  /is help (topic/command)")
            minetest.chat_send_player(name, "Topics")
            minetest.chat_send_player(name, " + create    Covers creation and destruction")
            minetest.chat_send_player(name, " + teleport  Covers teleporting, and locking")
            minetest.chat_send_player(name, " + vault     Covers island vault")
            minetest.chat_send_player(name, " + mailbox   Covers the island mailbox")
            minetest.chat_send_player(name, " + level     Covers island level")
            minetest.chat_send_player(name, " + co_op     Covers co-op")
            minetest.chat_send_player(name, " + invite    Covers inventations, promotion and demotion")
            minetest.chat_send_player(name, " + kick      Covers kicking, and banning")
            if adv_sb.temp.cmds["warp"] ~= nil then
                minetest.chat_send_player(name, " + warp      Covers visiting, and visitors")
            else
                minetest.chat_send_player(name, " + warp      Covers visiting, and visitors (This feature is disabled)")
            end
            if adv_sb.temp.cmds["generator"] ~= nil then
                minetest.chat_send_player(name, " + generator Covers ore generators")
            else
                minetest.chat_send_player(name, " + generator Covers ore generators (This feature is disabled)")
            end
            if adv_sb.temp.cmds["minion"] ~= nil then
                minetest.chat_send_player(name, " + minion    Covers minions")
            else
                minetest.chat_send_player(name, " + minion    Covers minions (This feature is disabled)")
            end
        else
            local topic = args[1]:lower()
            if topic == "create" then
                minetest.chat_send_player(name, "/is create (island_name)")
                minetest.chat_send_player(name, "   Creates an island, as long as you do not own one or are a member of one")
                minetest.chat_send_player(name, "/is destroy")
                minetest.chat_send_player(name, "   Destroys your island, must be owner and there must be no members (Blocks are removed)")
            elseif topic == "teleport" then
                minetest.chat_send_player(name, "All teleport commands have a 5 second teleport delay")
                minetest.chat_send_player(name, "/is")
                minetest.chat_send_player(name, "   Teleports to your island (equal to '/is go " .. name .. "')")
                if adv_sb.temp.settings.teleport.allowed_when_falling then
                    minetest.chat_send_player(name, "   (Teleport isn't canceled when falling)")
                end
                minetest.chat_send_player(name, "/is go <name>")
                minetest.chat_send_player(name, "   Teleports to island home with given name")
                if adv_sb.temp.settings.teleport.allowed_when_falling then
                    minetest.chat_send_player(name, "   (Teleport isn't canceled when falling)")
                end
                minetest.chat_send_player(name, "/is sethome (name)")
                minetest.chat_send_player(name, "   Sets a island home with given name to current location and direction")
                minetest.chat_send_player(name, "   When name is ommited your name, '" .. name .. "', is used")
                minetest.chat_send_player(name, "/is rmhome <name>")
                minetest.chat_send_player(name, "   Removes a island home with given name")
                minetest.chat_send_player(name, "   Can't be ommited or your name '" .. name .. "'")
            elseif topic == "vault" then
                minetest.chat_send_player(name, "An island can have a island vault, a chest essentially")
                minetest.chat_send_player(name, "   Island vaults can be accessed from anywhere, and can be configured (via '/is settings')")
                if adv_sb.gamemode == "mcl" then
                    minetest.chat_send_player(name, "   Vaults don't physically exist, can be multiple pages, and all pages are 6 by 9")
                else
                    minetest.chat_send_player(name, "   Vaults don't physically exist, can be multiple pages, and all pages are 3 by 8")
                end
                minetest.chat_send_player(name, "/is vault")
                minetest.chat_send_player(name, "   Opens the island vault settings")
                minetest.chat_send_player(name, "/is vault <page>")
                minetest.chat_send_player(name, "   Opens the island vault to page number")
            elseif topic == "level" then
                minetest.chat_send_player(name, "Islands have 'level', each block depending on type adds points")
                minetest.chat_send_player(name, "   Each level is (100 * level) + (10 * level) points (So a level 3 island would need 330 points)")
                minetest.chat_send_player(name, "   When an island reaches that number of points it levels up (leveling up might give items, or unlock features)")
                minetest.chat_send_player(name, "   Island levels may unlock upgrades (from more minions, faster teleporting, more island members, and more)")
                minetest.chat_send_player(name, "/is level")
                minetest.chat_send_player(name, "   Checks your island's level (and returns the amount of points till next levelup)")
            elseif topic == "co_op" or topic == "coop" then
                minetest.chat_send_player(name, "Island Co-op is a temporary privledge (Useful for quick short visits where help was/is needed)")
                minetest.chat_send_player(name, "   Multiple islands can co-op a player")
                minetest.chat_send_player(name, "   Co-op players can break and place blocks, open chests")
                minetest.chat_send_player(name, "   Co-op players will remain co-op'ed until the island owner disconnects")
                minetest.chat_send_player(name, "   Only the island owner can co-op a player")
                minetest.chat_send_player(name, "/is coop <name>")
                minetest.chat_send_player(name, "   Co-op's the given player")
                minetest.chat_send_player(name, "/is decoop <name>")
                minetest.chat_send_player(name, "   Removes Co-op on the given player")
                minetest.chat_send_player(name, "/is decoop_all")
                minetest.chat_send_player(name, "   Removes all Co-op'ed players")
            elseif topic == "invite" then
                minetest.chat_send_player(name, "Islands can have varying amounts of players on them (as members)")
                minetest.chat_send_player(name, "   Typically every 5 island levels unlocks another member (so a level 15 island will have 3 additional from the starting amount)")
                minetest.chat_send_player(name, "   Islands start with " .. tostring(adv_sb.temp.settings.starting_members) .. " members (Upto a max of " .. tostring(adv_sb.temp.settings.max_members) .. " at level " .. tostring((adv_sb.temp.settings.max_members - adv_sb.temp.settings.starting_members) * 5) .. ")")
                minetest.chat_send_player(name, "/is invite <name>")
                minetest.chat_send_player(name, "   Sends an invite to a player (They will have upto 15 minutes to accept it)")
                minetest.chat_send_player(name, "   Players can only be a member of one island, they must issue '/island leave' or /island destroy' to leave their current island first")
                minetest.chat_send_player(name, "   (This command can only be issued by the island owner)")
                minetest.chat_send_player(name, "/is invite_cancel <name>")
                minetest.chat_send_player(name, "   Cancels an invite to a player")
                minetest.chat_send_player(name, "   (This command can only be issued by the island owner)")
                minetest.chat_send_player(name, "Island members can be promoted to gain access to restricted features (such as island kick or ban)")
                minetest.chat_send_player(name, "/is promote <name>")
                minetest.chat_send_player(name, "   Promotes a island member to island officer (given player must already be a island member)")
                minetest.chat_send_player(name, "   (This command can only be issued by the island owner)")
                minetest.chat_send_player(name, "/is demote <name>")
                minetest.chat_send_player(name, "   Demotes a island officer to a island member (given player must already be a island officer)")
                minetest.chat_send_player(name, "   (This command can only be issued by the island owner)")
            elseif topic == "kick" or topic == "ban" then
                minetest.chat_send_player(name, "At some point a player (either a visitor, member, or officer) might be annoying or bad")
                minetest.chat_send_player(name, "/is kick <name>")
                minetest.chat_send_player(name, "   Kicks them off the island, if the current island isn't theirs (or they aren't a member/officer) they are teleported to their own island")
                minetest.chat_send_player(name, "   If they were a member of officer they are actually kicked from the server")
                minetest.chat_send_player(name, "   (This command can only be issued by the island owner and island officers)")
                minetest.chat_send_player(name, "At some point the same player (either a visitor, member, or officer) might be too annoying or really evil")
                minetest.chat_send_player(name, "/is ban <name>")
                minetest.chat_send_player(name, "   This will automatically kick the visitor off the island")
                minetest.chat_send_player(name, "   This will demote any member and officer to visitor then kicks them if they are online")
                minetest.chat_send_player(name, "   If it was a member or officer they are teleported to the nearest unlocked island on login")
                minetest.chat_send_player(name, "   (This command can only be issued by the island owner)")
                minetest.chat_send_player(name, "At some point you'll forget who you've banned and want to allow someone on your island")
                minetest.chat_send_player(name, "/is banlist")
                minetest.chat_send_player(name, "   Lists all players banned from this island")
                minetest.chat_send_player(name, "   (This command can be issued by all island members, officers and the owner)")
                minetest.chat_send_player(name, "At some point you'll forgive the player who was annoying or really evil")
                minetest.chat_send_player(name, "/is unban <name>")
                minetest.chat_send_player(name, "   Lifts the ban on the player")
                minetest.chat_send_player(name, "   (This command can only be issued by the island owner)")
            elseif topic == "mail" or topic == "mailbox" then
                minetest.chat_send_player(name, "All islands can receive 'donations' and messages from non-members")
                minetest.chat_send_player(name, "/is mail    or   /is mailbox")
                minetest.chat_send_player(name, "   Opens the island mail/mailbox GUI")
                minetest.chat_send_player(name, "   Here you can send messages to another island")
                minetest.chat_send_player(name, "   (Island owners and officers can send/recieve messages)")
                minetest.chat_send_player(name, "   (An exception is made: If you are not appart of an island you can't send messages)")
                minetest.chat_send_player(name, "   You can also send items to another island")
                minetest.chat_send_player(name, "   (Only island owners can retrieve items)")
                minetest.chat_send_player(name, "   (An exception is made: If you are not appart of an island you can't send items)")
            else
                minetest.chat_send_player(name, "Unknown topic, try '/island help' for a list of topics.")
            end
        end
    end
}

adv_sb.register_subcmd = function (name, func)
    adv_sb.log("sub-command '" .. name .. "' registered")
    adv_sb.temp.cmds[name] = func
end

local island_func = function (name, param)
    local parts = params:split(" ") -- Gets all parameters
    if #parts == 0 then
        return adv_sb.temp.cmds["help"].func(name, {})
    end
    local subcmd = parts[1]:lower()
    local args = {}
    for i, v in ipairs(parts) do
        if i ~= 1 then
            table.insert(args, v)
        end
    end
    for subcmd, func in adv_sb.temp.cmds do
        if scmd == subcmd then
            return func(name, args)
        end
    end
    return false, "No such command (Try '/island help')"
end

minetest.register_chatcommand("island", {
    privs = {
        shout = true,
    },
    description = "Access Advanced Skyblocks",
    func = island_func,
})

minetest.register_chatcommand("is", {
    privs = {
        shout = true,
    },
    description = "Access Advanced Skyblocks",
    func = island_func,
})
