
adv_sb.request_telport = function (player, pos, delay)
    if adv_sb.temp.tp == nil then
        adv_sb.temp.tp = {}
    end
    if player == nil then
        return false
    end
    adv_sb.temp.tp[player:get_player_name()] = {
        pos = minetest.pos_to_string(pos),
        delay = delay,
        start = time.now()
    }
    minetest.chat_send_player(player:get_player_name(), "Teleporting in " .. time.str_format(adv_sb.temp.tp[player:get_player_name()].start))
    return true
end

adv_sb.cancel_teleport = function (player)
    if adv_sb.temp.tp == nil then
        adv_sb.temp.tp = {}
    end
    if player == nil then
        return false
    end
    if adv_sb.temp.tp[player:get_player_name()] ~= nil then
        adv_sb.temp.tp[player:get_player_name()] = nil
        return true
    end
    return false
end

adv_sb.has_teleport = function (player)
    if adv_sb.temp.tp == nil then
        adv_sb.temp.tp = {}
    end
    if player == nil then
        return false
    end
    return adv_sb.temp.tp[player:get_player_name()] == nil
end

adv_sb.get_teleport = function (player)
    if adv_sb.temp.tp == nil then
        adv_sb.temp.tp = {}
    end
    if player == nil then
        return nil
    end
    return adv_sb.temp.tp[player:get_player_name()]
end
