--[[
===================================================
    ___       __                                __
   /   | ____/ /   ______ _____  ________  ____/ /
  / /| |/ __  / | / / __ `/ __ \/ ___/ _ \/ __  /
 / ___ / /_/ /| |/ / /_/ / / / / /__/  __/ /_/ /
/_/  |_\__,_/ |___/\__,_/_/ /_/\___/\___/\__,_/

   _____ __         __    __           __
  / ___// /____  __/ /_  / /___  _____/ /_______
  \__ \/ //_/ / / / __ \/ / __ \/ ___/ //_/ ___/
 ___/ / ,< / /_/ / /_/ / / /_/ / /__/ ,< (__  )
/____/_/|_|\__, /_.___/_/\____/\___/_/|_/____/
          /____/
===================================================
]]

string.plural = function(timevalue, field_name)
    if timevalue > 1 then
        return field_name .. "s"
    end
    return field_name
end

time = {
    ---Obtains current UNIX time, returning a UNIX timestamp
    ---@return integer
    now = function ()
        return os.time(os.date("*t"))
    end,
    ---Obtains time since given UNIX timestamp (returns a duration timestamp)
    ---@param stamp integer
    ---@return integer
    since = function(stamp)
        local now = time.now()
        return math.abs(now - stamp)
    end,
    ---Formats into a table the weeks, days, hours, minutes, and seconds within the timestamp
    --- Assuming given timestamp is a duration (obtainable by time.since)
    ---@param timestamp integer
    ---@return table
    format = function(timestamp)
        local minutes = math.floor(timestamp / 60)
        timestamp = timestamp - (minutes * 60)
        local hours = math.floor(minutes / 60)
        minutes = minutes - (hours * 60)
        local days = math.floor(hours / 24)
        hours = hours - (days * 24)
        local weeks = math.floor(days / 7)
        days = days - (weeks * 7)
        return {week=weeks, day=days, hour=hours, minute=minutes, second=timestamp}
    end,
    ---Formats into a string the given timestamp (assuming that it is a duration)
    --- i.e. 64 would return "1m 4s"
    ---@param timestamp integer
    ---@return string
    str_format = function(timestamp)
        local t = time.format(timestamp)
        local result = ""
        if t.week ~= 0 then
            result = result .. t.week .. "w"
        end
        if t.day ~= 0 then
            if result ~= "" then
                result = result .. " "
            end
            result = result .. t.day .. "d"
        end
        if t.hour ~= 0 then
            if result ~= "" then
                result = result .. " "
            end
            result = result .. t.hour .. "h"
        end
        if t.minute ~= 0 then
            if result ~= "" then
                result = result .. " "
            end
            result = result .. t.minute .. "m"
        end
        if t.second ~= 0 then
            if result ~= "" then
                result = result .. " "
            end
            result = result .. t.second .. "s"
        end
        return result
    end,
    ---Similar to time.str_format except extends the duration text
    --- i.e. 64 would return "1 minute, 4 seconds"
    ---@param timestamp integer
    ---@return string
    string_format = function(timestamp)
        local t = time.format(timestamp)
        local result = ""
        if t.week ~= 0 then
            result = result .. t.week .. " " .. laston.plural(t.week, "week")
        end
        if t.day ~= 0 then
            if result ~= "" then
                result = result .. ", "
            end
            result = result .. t.day .. " " .. laston.plural(t.day, "day")
        end
        if t.hour ~= 0 then
            if result ~= "" then
                result = result .. ", "
            end
            result = result .. t.hour .. " " .. laston.plural(t.hour, "hour")
        end
        if t.minute ~= 0 then
            if result ~= "" then
                result = result .. ", "
            end
            result = result .. t.minute .. " " .. laston.plural(t.minute, "minute")
        end
        if t.second ~= 0 then
            if result ~= "" then
                result = result .. ", "
            end
            result = result .. t.second .. " " .. laston.plural(t.second, "second")
        end
        return result
    end
}

adv_sb = {
    version = "1.0",
    gamemode = "???",
    store = minetest.get_mod_storage(),
    temp = {},
}

if minetest.registered_nodes["default:stone"] ~= nil then
    adv_sb.gamemode = "mtg"
elseif minetest.registered_nodes["mcl_core:stone"] ~= nil then
    adv_sb.gamemode = "mcl"
end

adv_sb.log = function (msg)
    if type(msg) == "table" then
        msg = minetest.serialize(msg)
    end
    minetest.log("action", "[adv_sb] " .. msg)
end

adv_sb.dofile = function (dir, file)
    local modpath = minetest.get_modpath(minetest.get_current_modname())
    if file == nil then
        dofile(modpath .. DIR_DELIM .. dir .. ".lua")
    else
        dofile(modpath .. DIR_DELIM .. dir .. DIR_DELIM .. file .. ".lua")
    end
end


adv_sb.log("===================================================")
adv_sb.log("    ___       __                                __")
adv_sb.log("   /   | ____/ /   ______ _____  ________  ____/ /")
adv_sb.log("  / /| |/ __  / | / / __ `/ __ \\/ ___/ _ \\/ __  /")
adv_sb.log(" / ___ / /_/ /| |/ / /_/ / / / / /__/  __/ /_/ /")
adv_sb.log("/_/  |_\\__,_/ |___/\\__,_/_/ /_/\\___/\\___/\\__,_/")
adv_sb.log("")
adv_sb.log("   _____ __         __    __           __")
adv_sb.log("  / ___// /____  __/ /_  / /___  _____/ /_______")
adv_sb.log("  \\__ \\/ //_/ / / / __ \\/ / __ \\/ ___/ //_/ ___/")
adv_sb.log(" ___/ / ,< / /_/ / /_/ / / /_/ / /__/ ,< (__  )")
adv_sb.log("/____/_/|_|\\__, /_.___/_/\\____/\\___/_/|_/____/")
adv_sb.log("          /____/")
adv_sb.log("===================================================")
adv_sb.log("Loading Teleportation ...")
local stmp = time.now()
adv_sb.dofile("teleport")
adv_sb.dofile("tp_tracker")
stmp = time.since(stmp)
adv_sb.log("Loaded Teleportation (took " .. time.str_format(stmp) .. ")")

adv_sb.log("Loading Chat Commands ...")
stmp = time.now()
adv_sb.dofile("chat_command")
stmp = time.since(stmp)
adv_sb.log("Loaded Chat Commands (took " .. time.str_format(stmp) .. ")")
adv_sb.log("===================================================")
