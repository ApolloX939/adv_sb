
local CHECK_INTERVAL = 1

local checkTimer = 0

local joinleave_player = function (player)
    adv_sb.cancel_teleport(player)
end

minetest.register_on_joinplayer(joinleave_player)
minetest.register_on_leaveplayer(joinleave_player)

--[[minetest.register_on_chat_message(function (name, msg)
    if home_point.players[name] then
        home_point.players[name].lastAction = minetest.get_gametime()
        minetest.log("action", "[home_point] "..name.." chatted")
    end
end)]]

minetest.register_on_punchplayer(function (player, hitter)
    local name = player:get_player_name()
    local hit = hitter:get_player_name()
    if adv_sb.has_teleport(player) then
        minetest.chat_send_player(name, "Teleport Canceled, PVP!")
    end
    adv_sb.cancel_teleport(player)
    if adv_sb.has_teleport(hitter) then
        minetest.chat_send_player(hit, "Teleport Canceled, PVP!")
    end
    adv_sb.cancel_teleport(hitter)
end)

local questionTeleport = function (player, name, info, curTime)
    if info.start + info.delay <= curTime then
        -- Teleport
        player:set_pos(minetest.string_to_pos(info.pos))
        player:add_velocity({x = 0, y = (math.abs(player:get_velocity().y))*0.20, z = 0}) -- All this because set_velocity isn't supported
        minetest.chat_send_player(name, "Teleported")
    else
        if info.request then
            local diff = (info.start + info.delay) - curTime
            if diff <= 5 then
                minetest.chat_send_player(name, "Teleporting in "..tostring(diff).."s")
            end
        end
    end
end

minetest.register_globalstep(function (dtime)
    local curTime = minetest.get_gametime()
    checkTimer = checkTimer + dtime

    local checkNow = checkTimer >= CHECK_INTERVAL
    if checkNow then
        checkTimer = checkTimer - CHECK_INTERVAL
    end

    if adv_sb.temp.tp == nil then
        adv_sb.temp.tp = {}
    end

    for name, info in pairs(adv_sb.temp.tp) do
        if info ~= nil then
            local player = minetest.get_player_by_name(name)
            if player then
                for _, keyPress in pairs(player:get_player_control()) do
                    if keyPress then
                        if info ~= nil then
                            adv_sb.temp.tp[name] = nil
                            minetest.chat_send_player(name, "Teleport Canceled, you moved!")
                        end
                    end
                end

                if checkNow then
                    questionTeleport(player, name, info, curTime)
                end
            else -- Clean up invalids
                adv_sb.temp.tp[name] = nil
            end
        end
    end
end)
