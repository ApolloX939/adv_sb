
adv_sb.register_subcmd("go",
    func = function (player, params)
        adv_sb.home.go_home(player, params[1])
    end
)

adv_sb.register_subcmd("sethome",
    func = function (player, params)
        if #params == 0 then
            adv_sb.home.set_home(player, player:get_player_name())
        else
            adv_sb.home.set_home(player, param[1])
        end
    end
)
