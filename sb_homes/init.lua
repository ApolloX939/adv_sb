
adv_sb.log("Loading Island Homes...")
local stmp = time.now()

adv_sb.dofile("api.lua")
adv_sb.dofile("chat_commands.lua")

stmp = time.since(stmp)
adv_sb.log("Loaded Island Homes in " .. time.str_format(stmp))
