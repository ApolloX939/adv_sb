
adv_sb.homes.get_home = function (player, home_name)
    local name = player:get_player_name()
    local homes = minetest.deserialize(adv_sb.store:get_string("homes")) or {}
    if homes[name] == nil then
        return nil
    end
    for hn, pos in pairs(homes[name]) do
        if hn == home_name then
            return minetest.string_to_pos(pos)
        end
    end
    return nil
end

adv_sb.homes.set_home = function (player, home_name)
    local name = player:get_player_name()
    local homes = minetest.deserialize(adv_sb.store:get_string("homes")) or {}
    if homes[name] == nil then
        homes[name] = {}
    end
    homes[name][home_name] = minetest.pos_to_string(player:get_pos())
    adv_sb.store:set_string("homes", minetest.serialize(homes))
end

adv_sb.homes.go_home = function (player, home_name)
    local pos = adv_sb.homes.get_home(player, home_name)
    if pos ~= nil then
        adv_sb.request_telport(player, pos, 5)
    end
end
